clear;
clc;
close all;

hero_dir = getenv('HERO_DIRECTORY');
addpath(genpath(hero_dir))

% Which Falkner-Skan case do you want to run?
beta = 0;
% beta = 0.1;
% beta = -0.1;

% Do you want to create the plot of all three neutral stability curves?
plot_all_neut_curves = false;

if beta == 0
    fs_data_filename = [hero_dir, '/cases/project_1.1.3/mat_files/fs_beta_0p0_proj1p1p3.mat'];
    os_data_filename = [hero_dir, '/cases/project_1.1.3/mat_files/os_beta_0p0_proj1p1p3.mat'];
elseif beta == 0.1
    fs_data_filename = [hero_dir, '/cases/project_1.1.3/mat_files/fs_beta_0p1_proj1p1p3.mat'];
    os_data_filename = [hero_dir, '/cases/project_1.1.3/mat_files/os_beta_0p1_proj1p1p3.mat'];
elseif beta == -0.1
    fs_data_filename = [hero_dir, '/cases/project_1.1.3/mat_files/fs_beta_n0p1_proj1p1p3.mat'];
    os_data_filename = [hero_dir, '/cases/project_1.1.3/mat_files/os_beta_n0p1_proj1p1p3.mat'];
else
    error('not investigated yet!')
end
fs_data = load(fs_data_filename);
sys = fs_data.sys_out;

% Numerics
sys.eigensolver = 2;    % 1: global, 2: local
sys.arnoldi_Ny = 1;
sys.Ny = 175;
sys.y_domain_scheme = 0;     % 0: malik-clustering; 1: equally-spaced
sys.y_crit = 3.6;
sys.y_max = 110;

sys.filter = true;
sys.silent = true;
sys.plot_eigenfunctions = false;
% sys.plot_eigenfunctions = true;  % for debugging!
sys.plot_spectrum = false;

Rmax = 3100;
Rmin = 50;
alphamax = 0.3466;
alphamin = 0.02;

% Stability
% Note: The initial guess for the first (R, alpha) comes by solving the appropriate
% temporal problem in a separate script and ensuring that the mode shape is indeed a
% Tollmien-Schlichting wave. sys.eigs_sigma_search is the corresponding omega (eigenvalue)
if beta == 0
    R_part1 = linspace(275, Rmax, 75);
    R_part2 = linspace(Rmax, Rmin, 75);
    R_part3 = linspace(Rmin, 275, 10);
    alpha_part1 = linspace(0.170, alphamax, 30);
    alpha_part2 = linspace(alphamax, alphamin, 50);
    alpha_part3 = linspace(alphamin, 0.170, 20);
    R = [301.7, R_part1, R_part2, R_part3];
    alpha = [0.175; alpha_part1.'; alpha_part2.'; alpha_part3.'];
    sys.eigs_sigma_search = 0.0693 - 0.0000i; % hardcode: associated with (R, alpha) = (301.7, 0.175)
elseif beta == 0.1
    R_part1 = linspace(2000, Rmax, 50);
    R_part2 = linspace(Rmax, 450, 50);
    R_part3 = linspace(450, Rmin, 10);
    alpha_part1 = linspace(0.08, 0.2, 60);
    alpha_part2 = linspace(0.2, alphamax, 20);
    alpha_part3 = linspace(alphamax, 0.2, 20);
    alpha_part4 = linspace(0.2, 0.05, 50);
    alpha_part5 = linspace(0.05, alphamin, 5);
    R = [2000, R_part1, R_part2, R_part3];
    alpha = [0.09; alpha_part1.'; alpha_part2.'; alpha_part3.'; alpha_part4.'; alpha_part5.'];
    sys.eigs_sigma_search = 0.0239 - 0.0000i; % hardcode: associated with (R, alpha) = (2000, 0.09)
elseif beta == -0.1
    R_part1 = linspace(400, Rmax, 60);
    R_part2 = linspace(Rmax, Rmin, 60);
    R_part3 = linspace(Rmin, 400, 20);
    alpha_part1 = linspace(0.0625, alphamax, 35);
    alpha_part2 = linspace(alphamax, alphamin, 40);
    alpha_part3 = linspace(alphamin, 0.0625, 10);
    R = [400, R_part1, R_part2, R_part3];
    alpha = [0.0625; alpha_part1.'; alpha_part2.'; alpha_part3.'];
    sys.eigs_sigma_search = 0.0179 + 0.0000i; % hardcode: associated with (R, alpha) = (400, 0.0625)
end
N_R = length(R);
N_alpha = length(alpha);
F = zeros(N_R, N_alpha);
omega = F;
cph = F;

%% Solve the 'snake-like' daisy-chained search
tic;
for ii = 1:N_R
    sys.R = R(ii);
    for jj = 1:N_alpha
        sys.alpha = alpha(jj);
        
        % Solve the temporal problem
        sys_out = os_hero_main(sys, false);
        
        % Store output
        omega(ii,jj) = sys_out.omega;
        F(ii,jj) = real(omega(ii,jj))/sys_out.R;
        cph(ii,jj) = sys_out.cph;
        
        % Update next eigs() guess
        if jj == N_alpha
            % Since alpha is getting reset to the first value, we reset the initial guess as well
            sys.eigs_sigma_search = omega(ii,1);
        else
            % Simply incrementing in alpha
            sys.eigs_sigma_search = omega(ii,jj);
        end
    end
end
toc;
save(os_data_filename, 'omega', 'F', 'cph', 'R', 'alpha')

if ~plot_all_neut_curves
    return
end

%% Plotting
hero_dir = getenv('HERO_DIRECTORY');
addpath(genpath(hero_dir))
mat_files = dir([hero_dir, '/cases/project_1.1.3/mat_files/os_beta_*']);
beta_list = {' 0.0', ' 0.1', ' -0.1'};
beta_str = strcat('\beta =', beta_list);
fs_labels = 13;
close all
N_plots = length(mat_files);
ax1 = figure(1);

cph_largest = -inf;

for ii = 1:N_plots
    clear omega F cph R alpha
    load([mat_files(ii).folder, '/', mat_files(ii).name])
    cph_largest_wrk = max(max(imag(cph)));
    if cph_largest_wrk > cph_largest
        cph_largest = cph_largest_wrk;
    end
end

for ii = 1:N_plots
    clear omega F cph R alpha
    load([mat_files(ii).folder, '/', mat_files(ii).name]) % 'omega', 'F', 'cph', 'R', 'alpha' are loaded'
    
    N_alpha = length(alpha);
    N_R = length(R);
    R_2D = repmat(R, [N_alpha, 1]);
    n_levels = 256;
    alpha_r = real(alpha);
    alpha_r_2D = repmat(alpha_r, [1, N_R]);
    subplot(N_plots, 1, ii)
    contourf(R_2D, alpha_r_2D, imag(cph).', n_levels, 'edgecolor', 'none');
    hold on;
    contour(R_2D, alpha_r_2D, imag(cph).', [0 0] ...
        , 'linewidth', 1 ...
        , 'linestyle', ':' ...
        , 'color', [0.5 0.5 0.5] ...
        );  % Light gray dots on the neutral curve
    
    ci_maph = colormap(redblue(n_levels));
    caxis([-cph_largest cph_largest]);

    ax1.CurrentAxes.XScale = 'log';
    ylh = ylabel('\alpha_{r}');
    ylabel('\alpha_{r}','HorizontalAlignment','right','VerticalAlignment','middle','Rotation',0);

    if ii == N_plots
        xlabel('R', 'fontsize', fs_labels);
    else
        set(gca,'xticklabel',{[]});
    end
    set(gca, 'fontsize', fs_labels)
    grid minor
end
set(gcf,'position',[-959 401 960 1108]);
hp_last = get(subplot(N_plots,1,N_plots),'Position');
h1 = colorbar('Position', [0.912533333333333,0.111,0.0354,0.8141]);
set(get(h1,'title'), 'string', 'c_i');
box_x = 0.1375;
annotation(gcf,'textbox',...
    [box_x 0.716967509482529 0.126041666666667 0.0348375446690971],...
    'String',beta_str{1},...
    'FontSize',13,...
    'FitBoxToText','on',...
    'EdgeColor',[0.149019607843137 0.149019607843137 0.149019607843137],...
    'BackgroundColor',[1 1 1]);
annotation(gcf,'textbox',...
    [box_x 0.417328520312853 0.126041666666667 0.0348375446690971],...
    'String',beta_str{2},...
    'FontSize',13,...
    'FitBoxToText','on',...
    'EdgeColor',[0.149019607843137 0.149019607843137 0.149019607843137],...
    'BackgroundColor',[1 1 1]);
annotation(gcf,'textbox',[0.1375 0.118592058218989 0.126041666666667 0.034837544669097],...
    'String',beta_str{3},...
    'FontSize',13,...
    'FitBoxToText','on',...
    'EdgeColor',[0.149019607843137 0.149019607843137 0.149019607843137],...
    'BackgroundColor',[1 1 1]);

saveas(gcf,[hero_dir, '/cases/project_1.1.3/figures/neutral_stability_curves.png'])
