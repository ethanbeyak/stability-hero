clear;
% clc;
close all;

hero_dir = getenv('HERO_DIRECTORY');
addpath(genpath(hero_dir))
fs_data_filename = [hero_dir, '/cases/project_1.2/mat_files/blasius_basicstate_proj1p2.mat'];
fs_data = load(fs_data_filename);
sys = fs_data.sys_out;

% Numerics
sys.eigensolver = 2;    % 1: global, 2: local
sys.arnoldi_Ny = 1;
sys.Ny = 200;               % Should be 200 for a good solution
sys.y_domain_scheme = 0;    % 0: malik-clustering; 1: equally-spaced
sys.y_crit = 3.23;
sys.y_max = 100;
sys.nr_itmax = 200;      % Max number of newton-raphson iterations permitted
sys.nr_tol = 1e-10;     % Relative error required for convergence

% Stability
sys.filter = true;

% Streakline data
% Place computational smoke wire at y = 2 mm, x = 138 cm
sys.U_inf = 6.6;                % m/s
sys.streak_y0 = 2e-3;           % m
sys.streak_x0 = 138e-2;         % m
sys.f_ribbon = 39;              % Hz, frequency of ribbon
sys.nu = 14.8e-6;               % m^2/s, kinematic viscosity
sys.max_uprime_RMS_branchII_percent = 0.2; % 0.2% * U_inf
sys.time_span = linspace(0,0.0108,37);     % sec, time to span

sys_out = os_streak_main(sys);

%% Perturbations in time
u_prime = sys_out.u_prime;
v_prime = sys_out.v_prime;
y_dim = sys_out.y_dim;
Nt = length(sys.time_span);
close all
xbound = 0.019; % m/s -- symmetric
figure(1)
subplot(1,2,1)
for tt = 1:Nt
    plot(u_prime(:,tt), y_dim ...
        ,'color', magma(tt, Nt) ...
        ,'linewidth', 1.5);
    hold on;
end
xlabel('u'' (m/s)');
ylabel('y (m)');
ylim([0 0.035]);
xlim([-xbound xbound])
subplot(1,2,2)
for tt = 1:Nt
    plot(v_prime(:,tt), y_dim ...
        ,'color', magma(tt, Nt) ...
        ,'linewidth', 1.5);
    hold on;
end
xlabel('v'' (m/s)');
ylabel('y (m)');
ylim([0 0.035]);
xlim([-xbound xbound])
% set(gcf,'position',[-1919 401 1920 1108]);  % Full left monitor

%% Modified basic-state profiles
% Extract locals
U = sys_out.U;
U_inf = sys_out.U_inf;
nu = sys_out.nu;
streak_x0 = sys_out.streak_x0;
m = sys_out.m;
f = sys_out.f;
eta = sys_out.eta;
df_deta = sys_out.df_deta;

u_bar = U_inf*U;
% Eqn (1-1) from FURTHER SOLUTIONS OF THE FALKNER-SKAN EQUATION, K. STEWARTSON 1953
v_bar_tmp = (U_inf*nu/(2*streak_x0*(m+1)))^(1/2)*(f - (m-1)*eta.*df_deta);
v_bar = U_inf*interp1(eta, v_bar_tmp, y_dim, 'linear', 'extrap');

% Repeat over time
u_bar = repmat(u_bar, [1, Nt]);
v_bar = repmat(v_bar, [1, Nt]);

u = u_bar + u_prime;
v = v_bar + v_prime;    % No longer parallel flow if incorporating v_bar

figure(2)
subplot(1,2,1)
for tt = 1:Nt
    plot(u(:,tt), y_dim ...
        ,'color', magma(tt, Nt) ...
        ,'linewidth', 1.5);
    hold on;
end
xlabel('u'' (m/s)');
ylabel('y (m)');
ylim([0 0.035]);
subplot(1,2,2)
for tt = 1:Nt
    plot(v(:,tt), y_dim ...
        ,'color', magma(tt, Nt) ...
        ,'linewidth', 1.5);
    hold on;
end
xlabel('v'' (m/s)');
ylabel('y (m)');
ylim([0 0.035]);

%%
% figure
% plot(v_bar, y_dim ...
%     ,'color', 'k' ...
%     ,'linewidth', 1.5);

% plot(abs(sys_out.v_hat)/max(abs(sys_out.u_hat)), y_dim ...
%     ,'color', )
