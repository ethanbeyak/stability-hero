clear;
clc;
close all;

hero_dir = getenv('HERO_DIRECTORY');
addpath(genpath(hero_dir))

% Self-similar streamwise pressure gradient
sys.beta = 0;

% Numerics
sys.options.tol = 1e-10;
sys.options.iter_max = 500;
sys.options.N_eta = 350;
sys.options.eta_domain_scheme = 0;     % 0: malik-clustering; 1: equally-spaced
sys.options.eta_crit = 5;
sys.options.eta_max = 15;

% Plotting
sys.options.plot_basic_state = true;

% Run Falkner-Skan stability-hero
sys_out = fs_hero_main(sys);

% Final printing for wall boundary-conditions
fprintf('f'''' at the wall = %1.5f\n', sys_out.df_deta2(1));
fprintf('Displacement thickness = %1.5f\n', sys_out.eta_star);
fprintf('Momentum thickness = %1.5f\n', sys_out.theta_star);

save([hero_dir, '/cases/project_1.2/mat_files/blasius_basicstate_proj1p2.mat']);
