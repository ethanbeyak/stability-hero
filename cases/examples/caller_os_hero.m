clear;
clc;
close all;

hero_dir = getenv('HERO_DIRECTORY');
addpath(genpath(hero_dir))
fs_data_filename = [hero_dir, '/cases/fs_output.mat'];
fs_data = load(fs_data_filename);
sys = fs_data.sys_out;

% Numerics
sys.eigensolver = 2;    % 1: global, 2: local
sys.arnoldi_Ny = 1;
sys.Ny = 100;
sys.y_domain_scheme = 0;     % 0: malik-clustering; 1: equally-spaced
sys.y_crit = 3.6;
sys.y_max = 75;

sys.filter = true;

sys.plot_eigenfunctions = true;
sys.plot_spectrum = true;

% Stability
% F = 100*10^-6
sys.alpha = 0.121;
sys.R = 423.4;

sys_out = os_hero_main(sys);

save([hero_dir, '/cases/os_output.mat']);
