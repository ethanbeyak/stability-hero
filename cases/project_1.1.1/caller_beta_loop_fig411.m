clear;
clc;
close all;

%%
addpath(genpath(getenv('HERO_DIRECTORY')))

%% Solve Falkner-Skan at various beta values
% ---- input ---- %
save_figure = false;
lbeta = [-0.198838, -0.18, -0.1, 0.0, 0.3, 1.0];
sbeta = {' -0.198838', ' -0.18', ' -0.1', ' 0.0', ' 0.3', ' 1.0'};
tol = 1e-9;
iter_max = 500;
N_eta = 100;
eta_domain_scheme = 1;       % 0: malik-clustering, 1: equally-spaced.
eta_max = 15;
% ----  end  ---- %

sbeta = strcat('$\beta =', sbeta, '$');
N_runs = length(lbeta);

for ii = 1:N_runs
    
    sys.beta = lbeta(ii);
    
    sys.options.tol = tol;
    sys.options.iter_max = iter_max;
    sys.options.N_eta = N_eta;
    sys.options.eta_domain_scheme = eta_domain_scheme;
    sys.options.eta_max = eta_max;
    
    sys.options.plot_basic_state = false;
    
    sys_out{ii} = fs_hero_main(sys); %#ok<SAGROW>
    
end

%% Plotting
close all;
lw = 2;
fontsize = 13;

% f' vs. eta
figure(1)
subplot(1,2,1)
for ii = 1:N_runs
    plot(sys_out{ii}.eta, sys_out{ii}.df_deta, ...
        'color', magma(ii, N_runs), ...
        'linewidth', lw);
    hold on;
end
ylim([0, 1.2]);
xlim([0, 6]);
xlabel('\eta')
ylabel("$$f'$$", 'interpreter', 'latex')
rotate_ylabel(get(gca,'ylabel'))
grid on
% align the value of 1.0 on both vertical axes for the subplots
set(gca, 'position', [0.1300 0.1100 0.3347 0.7018]);
legend({sbeta{:}}, 'location', 'best', 'interpreter', 'latex', 'fontsize', fontsize)
legend boxoff

% f'' vs. eta
subplot(1,2,2)
for ii = 1:N_runs
    plot(sys_out{ii}.eta, sys_out{ii}.df_deta2, ...
        'color', magma(ii, N_runs), ...
        'linewidth', lw);
    hold on;
end
ylim([0, 1.4]);
xlim([0, 6]);
xlabel('\eta')
ylabel("$$f''$$", 'interpreter', 'latex', 'interpreter', 'latex')
rotate_ylabel(get(gca,'ylabel'))
grid on
legend({sbeta{:}}, 'location', 'best', 'interpreter', 'latex', 'fontsize', fontsize)
legend boxoff

% left monitor, bottom right corner
set(gcf,'position',[-959 401 960 508]);

%% Save the figure
if ~save_figure
    return
end
figname = 'falkner-skan_recreate_fig_4-11.png';
saveloc = [getenv('HERO_DIRECTORY'), '/cases/project_1.1.1/figures'];
if ~exist(saveloc, 'dir')
    mkdir(saveloc)
end
print(gcf, [saveloc, '/', figname], '-dpng', '-r300');
