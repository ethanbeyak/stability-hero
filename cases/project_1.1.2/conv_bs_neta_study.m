clear;
clc;
close all;

hero_dir = getenv('HERO_DIRECTORY');
addpath(genpath(hero_dir))

%% Run Basic-State while varying basic-state resolution
% Self-similar streamwise pressure gradient
sys.beta = 0.0;

% Numerics
sys.options.iter_max = 500;
sys.options.tol = 1e-10;
sys.options.eta_crit = 5;
sys.options.eta_domain_scheme = 0;
sys.options.eta_max = 15;

% Plotting
sys.options.plot_basic_state = false;

% Varying basic-state resolution
Neta_list = 2.^(8:14);
N_cases = length(Neta_list);
for ii = 1:N_cases
    sys.options.N_eta = Neta_list(ii);
    sys_out = fs_hero_main(sys);
    out_name = sprintf('FS_N%i.mat', Neta_list(ii));
    save([hero_dir, '/cases/project_1.1.2/mat_files/', out_name], 'sys_out');
end

%% Run Stability on these runs
clearvars -except Neta_list hero_dir N_cases

for ii = 1:N_cases
    in_name = sprintf('FS_N%i.mat', Neta_list(ii));
    fs_data_filename = [hero_dir, '/cases/project_1.1.2/mat_files/', in_name];
    fs_data = load(fs_data_filename);
    sys = fs_data.sys_out;
    
    % Numerics
    sys.eigensolver = 2; % 1: global, 2: local
    sys.arnoldi_Ny = 1;
    sys.append_uniform_flow = true;
    sys.append_interp_method = 'linear';
    sys.y_domain_scheme = 0;     % 0: malik-clustering; 1: equally-spaced
    sys.Ny = 500;
    sys.y_crit = 3.6;
    sys.y_max = 55;
    sys.calculate_fro_norm = true;
    
    % Stability inputs @ F = 100*10^-6
    sys.alpha = 0.121;
    sys.R = 423.4;
    
    % Plotting
    sys.filter = true;
    sys.plot_eigenfunctions = false;
    sys.plot_spectrum = false;
    
    sys_out{ii} = os_hero_main(sys);
end
save([hero_dir, '/cases/project_1.1.2/mat_files/os_output.mat']);

%% Comparing various tolerances to the most refined grid
sys_ref = sys_out{end};
interpreter = 'latex';
save_figure = true;
eig_patch_alpha = 0.1;
figure_filename = 'conv_bs_neta_figure.jpg';
x1 = inf;
x2 = -inf;

figure(1)
for ii = 1:N_cases-1
    sys_out{ii}.error_eigenvector = abs((sys_out{ii}.u_hat - sys_ref.u_hat)./sys_ref.u_hat);
    sys_out{ii}.error_eigenvector = sys_out{ii}.error_eigenvector(2:end-1);
    semilogx(sys_out{ii}.error_eigenvector, sys_out{ii}.y(2:end-1) ...
        , 'color', magma(ii, N_cases-1) ...
        , 'linewidth', 2 ...
        );
    hold on;
    x1_ith = min(sys_out{ii}.error_eigenvector);
    x2_ith = max(sys_out{ii}.error_eigenvector);
    if x1 > x1_ith
        x1 = x1_ith;
    end
    if x2 < x2_ith
        x2 = x2_ith;
    end
end
eigensolver_error = eps*sys_ref.fro_norm;
fprintf('Eigensolver error: %e\n', eigensolver_error);

xlim_left = 10^floor(log10(x1));  % get nearest orders of magnitude
xlim_right = 10^ceil(log10(x2));
ylim_bot = 0;
ylim_top = sys.y_max;

% Shade out the region which has less error than the eigensolver's error
create_patch(xlim_left, eigensolver_error, ylim_bot, ylim_top ...
    ,'FaceColor','black' ...
    ,'FaceAlpha', eig_patch_alpha ...
    ,'EdgeColor', 'none')

set(gcf, 'defaulttextinterpreter', interpreter);
lgnd_wrk = regexp(cellstr(num2str(Neta_list(1:end-1))), '\s+', 'split');
lgnd = strcat("$N_\eta =", " ", lgnd_wrk{1},'$');
lgnd{end+1} = '$\epsilon_{eig}$';
legend(lgnd ...
    , 'location', 'bestoutside' ...
    , 'interpreter', interpreter);
ylim([ylim_bot ylim_top]);
xlim([xlim_left xlim_right]);
set(gcf, 'position', [-959, 401, 960, 508]);  % left monitor, bottom right
xlabel('$|(\hat{u} - \hat{u}_{ref})/\hat{u}_{ref}|$');
ylabel('$y_{OS}$');
hold off

if save_figure
    saveas(gcf, [hero_dir, '/cases/project_1.1.2/figures/', figure_filename]);
end

%% Plot eigenfunctions
figure(2)
for ii = 1:N_cases
    plot(abs(sys_out{ii}.v_hat), sys_out{ii}.y ...
        , 'color', magma(ii, N_cases-1) ...
        , 'linewidth', 2 ...
        );
    hold on;
end
hold off
