clear;
clc;
close all;

hero_dir = getenv('HERO_DIRECTORY');
addpath(genpath(hero_dir))

%% Run basic-state
% Self-similar streamwise pressure gradient
sys.beta = 0.0;

N_eta = 2500;

% Numerics
sys.options.iter_max = 500;
sys.options.tol = 1e-10;
sys.options.eta_crit = 5;
sys.options.N_eta = N_eta;
sys.options.eta_domain_scheme = 0;
sys.options.eta_max = 15;

% Plotting
sys.options.plot_basic_state = false;

sys_out = fs_hero_main(sys);
out_name = sprintf('FS_N%i.mat', N_eta);
save([hero_dir, '/cases/project_1.1.2/mat_files/', out_name], 'sys_out');

%% Run stability while varying stability y_max
clearvars -except hero_dir N_eta

in_name = sprintf('FS_N%i.mat', N_eta);
fs_data_filename = [hero_dir, '/cases/project_1.1.2/mat_files/', in_name];
fs_data = load(fs_data_filename);
sys = fs_data.sys_out;

% Numerics
sys.eigensolver = 2; % 1: global, 2: local
sys.arnoldi_Ny = 1;
sys.append_uniform_flow = true;
sys.append_interp_method = 'linear';
sys.y_domain_scheme = 0;     % 0: malik-clustering; 1: equally-spaced
sys.y_crit = 3.6;
sys.y_max = 55;
sys.calculate_fro_norm = true;

% Stability inputs @ F = 100*10^-6
sys.alpha = 0.121;
sys.R = 423.4;

% Plotting
sys.filter = true;
sys.plot_eigenfunctions = false;
sys.plot_spectrum = false;

Ny_list = round(2.^(3:0.5:12));
N_cases = length(Ny_list);
for ii = 1:N_cases
    sys.Ny = Ny_list(ii);
    sys_out{ii} = os_hero_main(sys);
end
save([hero_dir, '/cases/project_1.1.2/mat_files/os_output.mat']);

%% Plot eigenfunctions
figure(2)
save_figure = true;
eigenfunction_filename = 'conv_stab_Ny_eigenfunctions_figure.jpg';
interpreter = sys_out{1}.interpreter;
for j = 1:N_cases
    
    u_hat = sys_out{j}.u_hat;
    v_hat = sys_out{j}.v_hat;
    cph = sys_out{j}.cph;
    y = sys_out{j}.y;
    fs = sys_out{j}.fontsize;
    lw = sys_out{j}.linewidth;
    ms = sys_out{j}.markersize;
    
    % Final plotting and output to the command window
    N_modes = size(u_hat, 2);
    
    for i = 1:N_modes
        u_hat_norm = abs(u_hat(:,i));
        v_hat_norm = abs(v_hat(:,i));
        subplot(1,2,1)
        % ... u
        plot(u_hat_norm./max(u_hat_norm), y ...
            , 'color', magma(j, N_cases) ...
            , 'linewidth', lw ...
            ); hold on
        subplot(1,2,2)
        % ... v
        plot(v_hat_norm./max(u_hat_norm), y ...
            , 'color', magma(j, N_cases) ...
            , 'linewidth', lw ...
            ); hold on
    end
end
set(gcf, 'defaulttextinterpreter', interpreter);
set(gcf, 'position', [-959, 401, 960, 508]);  % left monitor, bottom right
subplot(1,2,1)
lgnd_wrk = regexp(cellstr(num2str(Ny_list(1:end))), '\s+', 'split');
lgnd = strcat("$N_{y} =", " ", lgnd_wrk{1},'$');
title('$\hat{u}$', 'fontsize', fs)
xlabel('$|\hat{u}|/max(|\hat{u}|)$', 'fontsize', fs);
ylabel('$y_{OS}$', 'fontsize', fs);
ylim([0 inf])
subplot(1,2,2)
title('$\hat{v}$', 'fontsize', fs)
xlabel('$|\hat{v}|/max(|\hat{u}|)$', 'fontsize', fs);
ylim([0 inf])
legend(lgnd ...
    , 'location', 'best' ...
    , 'interpreter', interpreter);
set(gcf, 'position', [-959, 401, 960, 508]);  % left monitor, bottom right
hold off

if save_figure
    saveas(gcf, [hero_dir, '/cases/project_1.1.2/figures/', eigenfunction_filename]);
end

%% Plot eigenvalue convergence
sys_ref = sys_out{end};
eigenvalue_filename = 'conv_stab_Ny_eigenvalues_figure.jpg';
fs = 12;
figure(3)
eig_patch_alpha = 0.1;
cph_err = zeros(1,N_cases - 1);
for j = 1:N_cases - 1
    cph_err(j) = abs((sys_out{j}.cph - sys_ref.cph)/sys_ref.cph);
end
for j = 1:N_cases-1
    loglog(sys_out{j}.Ny, cph_err(j) ...
        , 'color', magma(j, N_cases - 1) ...
        , 'marker', 'd' ...
        , 'markersize', 10 ...
        );
    hold on;
end
for j = 1:N_cases-1
    loglog(sys_out{j}.Ny, cph_err(j) ...
        , 'color', magma(j, N_cases - 1) ...
        , 'marker', '+' ...
        , 'markersize', 10 ...
        );
    hold on;
end
eigensolver_error = eps*sys_ref.fro_norm;
fprintf('Eigensolver error: %e\n', eigensolver_error);

xlim_left = 10^floor(log10(min(Ny_list)));  % get nearest orders of magnitude
xlim_right = 10^ceil(log10(max(Ny_list)));
ylim_bot = 10^floor(log10(min(cph_err)));
ylim_top = 10^ceil(log10(max(cph_err)));

set(gcf, 'defaulttextinterpreter', interpreter);
set(gcf, 'position', [-959 1001 960 508]);  % left monitor, top right
lgnd_wrk = regexp(cellstr(num2str(Ny_list(1:end-1))), '\s+', 'split');
lgnd = strcat("$N_{y} =", " ", lgnd_wrk{1},'$');
ylabel('$|(c - c_{ref})/c_{ref}|$', 'fontsize', fs);
xlabel('$N_{y}$', 'fontsize', fs);
xlim([xlim_left xlim_right])
ylim([ylim_bot ylim_top])
legend(lgnd ...
    , 'location', 'best' ...
    , 'interpreter', interpreter ...
    , 'fontsize', fs);
rotate_ylabel()
set(gca,'position', [0.1656 0.1100 0.7394 0.8150]); % scoot over a bit for the rotated ylabel
hold off

if save_figure
    saveas(gcf, [hero_dir, '/cases/project_1.1.2/figures/', eigenvalue_filename]);
end
