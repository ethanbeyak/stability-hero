clear;
clc;
close all;

hero_dir = getenv('HERO_DIRECTORY');
addpath(genpath(hero_dir))
fs_data_filename = [hero_dir, '/cases/project_1.1.2/fs_output.mat'];
fs_data = load(fs_data_filename);
sys = fs_data.sys_out;

% Numerics
% sys.eigensolver = 1; % 1: global, 2: local
sys.eigensolver = 2; % 1: global, 2: local
sys.arnoldi_Ny = 1;
sys.append_uniform_flow = true;
% sys.append_uniform_flow = false;
sys.append_interp_method = 'linear';
sys.Ny_os = 1500; % necessary only if appending uniform flow
sys.y_os_max = 55; % necessary only if appending uniform flow

% Stability inputs
% F = 100*10^-6
sys.alpha = 0.121;
sys.R = 423.4;

% Plotting
sys.filter = true;
sys.plot_eigenfunctions = true;
sys.plot_spectrum = true;

sys_out = os_hero_main(sys);

save([hero_dir, '/cases/project_1.1.2/os_output.mat']);