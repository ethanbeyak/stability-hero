clear;
clc;
close all;

hero_dir = getenv('HERO_DIRECTORY');
addpath(genpath(hero_dir))

%% Run Basic-State while varying ode45 tolerance
% Self-similar streamwise pressure gradient
sys.beta = 0.0;

N_eta = 200;
eta_max = 15;

% Numerics
sys.options.iter_max = 500;
sys.options.N_eta = N_eta;
sys.options.eta_crit = 5;
sys.options.eta_domain_scheme = 0;
sys.options.eta_max = eta_max;

% Plotting
sys.options.plot_basic_state = false;

% Varying NR tol
tol_list = logspace(-6, -14, 9);
N_cases = length(tol_list);
for ii = 1:N_cases
    sys.options.tol = tol_list(ii);
    sys_out = fs_hero_main(sys);
    out_name = sprintf('FS_TOL%.0e_N%i_MAX%i.mat', ...
        tol_list(ii), N_eta, eta_max);
    save([hero_dir, '/cases/project_1.1.2/mat_files/', out_name], 'sys_out');
end

%% Run Stability on these runs
clearvars -except tol_list N_eta eta_max hero_dir N_cases

for ii = 1:N_cases 
    in_name = sprintf('FS_TOL%.0e_N%i_MAX%i.mat', ...
        tol_list(ii), N_eta, eta_max);
    fs_data_filename = [hero_dir, '/cases/project_1.1.2/mat_files/', in_name];
    fs_data = load(fs_data_filename);
    sys = fs_data.sys_out;
    
    % Numerics
    sys.eigensolver = 2; % 1: global, 2: local
    sys.arnoldi_Ny = 1;
    sys.append_uniform_flow = true;
    sys.append_interp_method = 'linear';
    sys.y_domain_scheme = 0;     % 0: malik-clustering; 1: equally-spaced
    sys.Ny = N_eta;
    sys.y_crit = 3.6;
    sys.y_max = 55;
    sys.calculate_fro_norm = true;
    
    % Stability inputs @ F = 100*10^-6
    sys.alpha = 0.121;
    sys.R = 423.4;
    
    % Plotting
    sys.filter = true;
    sys.plot_eigenfunctions = false;
    sys.plot_spectrum = false;
    
    sys_out{ii} = os_hero_main(sys);
end
save([hero_dir, '/cases/project_1.1.2/mat_files/os_output.mat']);

%% Comparing various tolerances to the most refined grid
sys_ref = sys_out{end};
interpreter = 'latex';
save_figure = true;
eig_patch_alpha = 0.1;
figure_filename = sprintf('conv_nr_tol_N%i_figure.jpg', N_eta);
x1 = inf;
x2 = -inf;

figure(1)
for ii = 1:N_cases-1
    sys_out{ii}.error_eigenvector = abs((sys_out{ii}.u_hat - sys_ref.u_hat)./sys_ref.u_hat);
    sys_out{ii}.error_eigenvector = sys_out{ii}.error_eigenvector(2:end-1);
    semilogx(sys_out{ii}.error_eigenvector, sys_out{ii}.y(2:end-1) ...
        , 'color', magma(ii, N_cases-1) ...
        , 'linewidth', 2 ...
        );
    hold on;
    x1_ith = min(sys_out{ii}.error_eigenvector);
    x2_ith = max(sys_out{ii}.error_eigenvector);
    if x1 > x1_ith
        x1 = x1_ith;
    end
    if x2 < x2_ith
        x2 = x2_ith;
    end
end
eigensolver_error = eps*sys_ref.fro_norm;
fprintf('Eigensolver error: %e\n', eigensolver_error);

xlim_left = 10^floor(log10(x1));  % get nearest orders of magnitude
xlim_right = 10^ceil(log10(x2));
ylim_bot = 0;
ylim_top = sys.y_max;

% Shade out the region which has less error than the eigensolver's error
create_patch(xlim_left, eigensolver_error, ylim_bot, ylim_top ...
    ,'FaceColor','black' ...
    ,'FaceAlpha', eig_patch_alpha ...
    ,'EdgeColor', 'none')

set(gcf, 'defaulttextinterpreter', interpreter);
lgnd_wrk = regexp(cellstr(num2str(tol_list(1:end-1))), '\s+', 'split');
lgnd = strcat("NR tol =", " ", lgnd_wrk{1});
lgnd{end+1} = '$\epsilon_{eig}$';
legend(lgnd ...
    , 'location', 'bestoutside' ...
    , 'interpreter', interpreter);
ylim([ylim_bot ylim_top]);
xlim([xlim_left xlim_right]);
set(gcf, 'position', [-959, 401, 960, 508]);  % left monitor, bottom right
xlabel('$|(\hat{u} - \hat{u}_{ref})/\hat{u}_{ref}|$');
ylabel('$y_{OS}$');

if save_figure
    saveas(gcf, [hero_dir, '/cases/project_1.1.2/figures/', figure_filename]);
end
