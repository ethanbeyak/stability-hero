% Debugging script for checking differentiation matrices against analytical derivatives
% To run this script, you have to be debugging in os_solve_sys after the matrices are
% generated.

figure(2)

subplot(2,2,1)
plot(y_os, cos(y_os), 'o'); hold on;
plot(y_os, D1*sin(y_os), 'x'); hold on;
title('First derivative');
hold off

subplot(2,2,2)
plot(y_os, -sin(y_os), 'o'); hold on;
plot(y_os, D2*sin(y_os), 'x'); hold on;
title('Second derivative');
hold off

subplot(2,2,3)
% plot(y_os, -cos(y_os), 'o'); hold on;
% plot(y_os, D3*sin(y_os), 'x'); hold on;
title('Third derivative');
hold off

subplot(2,2,4)
plot(y_os, sin(y_os), 'o'); hold on;
plot(y_os, D4*sin(y_os), 'x'); hold on;
title('Fourth derivative');
hold off
