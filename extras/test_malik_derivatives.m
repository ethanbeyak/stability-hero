% Debugging script for checking numerical differences against analytical derivatives
% of the clustered domain from Malik
% To run this script, you have to be debugging in os_solve_sys after the Malik grid is
% generated.

figure(3)
subplot(2,2,1)
delyeq_delyos = diff(y_eq)./diff(y_os);
yos_bd = y_os(1:end-1);
yos_fd = y_os(2:end);
plot(delyeq_delyos, yos_bd, 'o'); hold on;
plot(delyeq_delyos, yos_fd, 'd'); hold on;
plot(dy_eq_dy_os, y_os, 'k'); hold on;
title('First derivative');
hold off

subplot(2,2,2)
delyeq_delyos2_bd = diff(delyeq_delyos)./diff(yos_bd);
delyeq_delyos2_fd = diff(delyeq_delyos)./diff(yos_fd);
yos_bd2 = y_os(1:end-2);
yos_cd2 = y_os(2:end-1);
yos_fd2 = y_os(3:end);
plot(delyeq_delyos2_bd, yos_bd2, 'o'); hold on;
plot(delyeq_delyos2_fd, yos_fd2, 'x'); hold on;
plot(dy_eq_dy_os2, y_os, 'k'); hold on;
title('Second derivative');
hold off

subplot(2,2,3)
delyeq_delyos3_bd = diff(delyeq_delyos2_bd)./diff(yos_bd2);
delyeq_delyos3_fd = diff(delyeq_delyos2_fd)./diff(yos_fd2);
yos_bd3 = y_os(1:end-3);
yos_fd3 = y_os(4:end);
plot(delyeq_delyos3_bd, yos_bd3, 'o'); hold on;
plot(delyeq_delyos3_fd, yos_fd3, 'x'); hold on;
plot(dy_eq_dy_os3, y_os, 'k'); hold on;
title('Third derivative');
hold off

subplot(2,2,4)
delyeq_delyos4_bd = diff(delyeq_delyos3_bd)./diff(yos_bd3);
delyeq_delyos4_fd = diff(delyeq_delyos3_fd)./diff(yos_fd3);
yos_bd4 = y_os(1:end-4);
yos_fd4 = y_os(5:end);
plot(delyeq_delyos4_bd, yos_bd4, 'o'); hold on;
plot(delyeq_delyos4_fd, yos_fd4, 'x'); hold on;
plot(dy_eq_dy_os4, y_os, 'k'); hold on;
title('Fourth derivative');
hold off
