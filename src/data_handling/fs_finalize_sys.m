function sys_out = fs_finalize_sys(sys)
%FINALIZE_SYS

% Initialize output
sys_out = sys;

% Plot basic-state
if (sys.options.plot_basic_state)
    n_state = 4;    % {f, df_deta, df_deta2, df_deta3}
    ii = 1;
    lw = 2;
    figure
    plot(sys.f, sys.eta, ...
        'color', magma(ii, n_state + 1), ...
        'linewidth', lw); ii = ii + 1;
    hold on;
    plot(sys.df_deta, sys.eta, ...
        'color', magma(ii, n_state + 1), ...
        'linewidth', lw); ii = ii + 1;
    plot(sys.df_deta2, sys.eta, ...
        'color', magma(ii, n_state + 1), ...
        'linewidth', lw); ii = ii + 1;
    plot(sys.df_deta3, sys.eta, ...
        'color', magma(ii, n_state + 1), ...
        'linewidth', lw); %ii = ii + 1;
    xlim([-inf, max(max([sys.df_deta, sys.df_deta2]))]);
    ylim([0, sys.options.eta_max]);
    ylabel('\eta')
    legend({'f', 'f''', 'f''''''', 'f'''''''}, 'location', 'best')
end

% Dimensionless displacement thickness, (Eq. 4-75) in White, Frank
% integral(1 - f', eta, 0, inf)
sys_out.eta_star = sys.eta(end) - sys.f(end);

% Dimensionless momentum thickness, (Eq. 4-75) in White, Frank
% integral(f'*(1 - f'), eta, 0, inf)
sys_out.theta_star = (sys.df_deta2(1) - sys.beta*sys_out.eta_star) / (1 + sys.beta);

end
