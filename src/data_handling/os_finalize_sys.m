function sys_out = os_finalize_sys(sys)
%OS_FINALIZE_SYS Plot eigenfunctions and eigenvalue spectrum

sys_out = sys;

% Extract locals
u_hat = sys.u_hat;
v_hat = sys.v_hat;
cph = sys.cph;
y = sys.y;
fs = sys.fontsize;
lw = sys.linewidth;
ms = sys.markersize;

% Final plotting and output to the command window
N_modes = size(u_hat, 2);

% Eigenfunction plotting
% If filtering, these are post-filter
fc = 0; % figure-counter
if sys.plot_eigenfunctions
    fc = fc + 1;
    figure(fc)
    for i = 1:N_modes
        u_hat_norm = abs(u_hat(:,i));
        v_hat_norm = abs(v_hat(:,i));
        subplot(1,2,1)
        % ... u
        plot(u_hat_norm./max(u_hat_norm), y ...
            , 'color', magma(i, N_modes) ...
            , 'linewidth', lw ...
            ); hold on
        subplot(1,2,2)
        % ... v
        plot(v_hat_norm./max(u_hat_norm), y ...
            , 'color', magma(i, N_modes) ...
            , 'linewidth', lw ...
            ); hold on
        fprintf('Mode #%i: Re(c) = %.3f, Im(c) = %.3f\n', i, real(cph(i)), imag(cph(i)));
    end
    set(gcf, 'defaulttextinterpreter', sys.interpreter);
    subplot(1,2,1)
    legend(strcat('Mode', ' $', arrayfun(@num2str, 1:N_modes, 'UniformOutput', false), '$') ...
        ,'location', 'best' ...
        ,'fontsize', fs ...
        ,'interpreter', sys.interpreter ...
        );
    title('$\hat{u}$', 'fontsize', fs)
    xlabel('$|\hat{u}|/max(|\hat{u}|)$', 'fontsize', fs);
    ylabel('$y_{OS}$', 'fontsize', fs);
    ylim([0 inf])
    subplot(1,2,2)
    title('$\hat{v}$', 'fontsize', fs)
    xlabel('$|\hat{v}|/max(|\hat{u}|)$', 'fontsize', fs);
    ylim([0 inf])
    hold off
end

% Spectrum plotting
% If filtering, these are pre-filter.
if sys.plot_spectrum
    fc = fc + 1;
    if sys.filter
        % OVERRIDING filtered phase speeds with the entire spectrum
        cph = sys.cph_prefilter;
        cph_postfilter = sys.cph;
    end
    % Sort by smallest magnitude for colormap plotting
    cph_sorted = sort(cph);
    N_spectrum = length(cph_sorted);
    figure(fc)
    for i = 1:N_spectrum
        plot(cph_sorted(i) ...
            , 'color', magma(i, N_spectrum) ...
            , 'marker', 'o' ...
            , 'markersize', ms ...
            ); hold on
    end
    if sys.filter
        % Place crosses on eigenmodes that made it through the filter.
        N_spectrum_filtered = length(cph_postfilter);
        for i = 1:N_spectrum_filtered
            idx_prefilter = find(cph_sorted == cph_postfilter(i));
            plot(cph_postfilter(i) ...
                , 'color', magma(idx_prefilter, N_spectrum) ...
                , 'marker', '+' ...
                , 'markersize', ms ...
                ); hold on
        end
    end
    set(gcf, 'defaulttextinterpreter', sys.interpreter);
    title('Spectrum of Eigenvalues', 'fontsize', fs)
    xlabel('$c_{r}$', 'fontsize', fs);
    ylabel('$c_{i}$', 'fontsize', fs);
    % Trim out huge outliers (10^11, Inf, NaN's, etc...)
    cph_plotting = cph(abs(cph) < 100);
    xlim([1.1*min(real(cph_plotting)), max(real(cph_plotting)) + 1]);
    ylim([1.1*min(imag(cph_plotting)), max(imag(cph_plotting)) + 1]);
    hold off
end

end
