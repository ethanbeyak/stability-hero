function sys_out = os_finalize_streak(sys)

sys_out = sys;

% Extract locals
streak_x0 = sys.streak_x0;
streak_y0 = sys.streak_y0;
u_hat = sys.u_hat;
v_hat = sys.v_hat;
alpha = sys.alpha;
omega = sys.omega;
delta_r = sys.delta_r;
time_span = sys.time_span;
U_inf = sys.U_inf;
y = sys.y;
A = sys.A;

% Construct dimensional y
y_dim = y*delta_r;

% Construct some nondim. quantities
x_nondim = streak_x0/delta_r;
t_nondim = U_inf*time_span/delta_r;

% Ensure that t_nondim is a row for the upcoming reshaping!
t_nondim = t_nondim(:).';

% Reshape all vectors size [Ny, Nt]
Nt = length(t_nondim);
Ny = length(u_hat);
u_hat_2D = repmat(u_hat, [1, Nt]);
v_hat_2D = repmat(v_hat, [1, Nt]);
t_nondim_2D = repmat(t_nondim, [Ny, 1]);

% Rebuild perturbations
Theta_2D = alpha*x_nondim - omega*t_nondim_2D;
wave_2D = exp(1i*Theta_2D);
up_tmp = u_hat_2D.*wave_2D + conj(u_hat_2D.*wave_2D);
vp_tmp = v_hat_2D.*wave_2D + conj(v_hat_2D.*wave_2D);

% Find the up value at the smokewire at the initial time.
% Note that this y height is likely to not fall exactly on the grid, so we interpolate
up_smokewire = interp1(y_dim, up_tmp(:,1), streak_y0);

% Note that the amplitude A provided corresponds to the y height of the smokewire
% So we need to normalize these quantities such that the amplitude here is in fact A
up_normalized = up_tmp/abs(up_smokewire);
vp_normalized = vp_tmp/abs(up_smokewire);
u_prime = A*(up_normalized);  % m/s
v_prime = A*(vp_normalized);  % m/s

% To check that the normalization is happening correctly, create the following plots:
%{
clf
plot(abs(up_normalized(:,1)), y_dim ...
    , 'linestyle', '-' ...
    , 'linewidth', 1.5)
hold on
plot([0, 1], streak_y0*[1, 1] ...
    , 'color', 'k' ...
    , 'linestyle', '-.' ...
)
plot([1, 1], [0, streak_y0], 'k-.');
ylim([0 0.01])
xlabel('|u_{hat}| normalized')
ylabel('y (m)')
legend('t = 0', 'streakline y height', 'location', 'best')

figure(2)
plot(abs(u_prime(:,1)), y_dim ...
    , 'linestyle', '-' ...
    , 'linewidth', 1.5)
hold on
plot([0, A], streak_y0*[1, 1] ...
    , 'color', 'k' ...
    , 'linestyle', '-.' ...
)
plot([A, A], [0, streak_y0], 'k-.');
xlim([0 inf])
ylim([0 0.01])
xlabel('|u''| (m/s)')
ylabel('y (m)')
legend(sprintf('t = 0 with A = %.4f m/s', A), 'streakline y height', 'location', 'best')
%}

% Assign output
sys_out.u_prime = u_prime;
sys_out.v_prime = v_prime;
sys_out.y_dim = y_dim;

end
