function sys_out = fs_setup_sys(sys)
%SETUP_SYS

% TODO: Assess input validity (check if less than separation beta etc...)

% Initialize output
sys_out = sys;

% Set defaults for parameters not specified in the caller
% Flow properties
beta = 0;
sys = set_default(sys, 'beta', beta, ...
    ['Setting the beta-hartree parameter to ', num2str(beta)]);

% Numerics
tol = 1e-9;
iter_max = 250;
N_eta = 100;
eta_max = 25;
eta_domain_scheme = 0;     % 0: malik-clustering; 1: equally-spaced
sys.options = set_default(sys.options, 'tol', tol, ...
    ['Setting the numerical tolerance to ', num2str(tol)]);
sys.options = set_default(sys.options, 'iter_max', iter_max, ...
    ['Setting the maximum numerical iterations to ', num2str(iter_max)]);
sys.options = set_default(sys.options, 'N_eta', N_eta, ...
    ['Setting the eta-domain points to ', num2str(N_eta)]);
sys.options = set_default(sys.options, 'eta_max', eta_max, ...
    ['Setting the eta-domain maximum to ', num2str(eta_max)]);
sys.options = set_default(sys.options, 'eta_domain_scheme', eta_domain_scheme, ...
    'Setting the eta-domain scheme to malik-clustering.');
% Defaults specific to malik-clustering numerics
if (sys.options.eta_domain_scheme == 0)
    eta_crit = 4;
    sys.options = set_default(sys.options, 'eta_crit', eta_crit, ...
        ['Setting the malik-clustering critical parameter to ', num2str(eta_crit)]);
end

% Plotting
plot_basic_state = true;
sys.options = set_default(sys.options, 'plot_basic_state', plot_basic_state, ...
    'Once calculated, we will plot the basic-state profile');

% Set boundary conditions
sys.bc.f_wall = 0;
sys.bc.df_deta_wall = 0;
sys.bc.df_deta_fs = 1;
sys.bc.df_deta2_wall = get_df_deta2_guess(sys.beta);

% Define wall-normal domain
if sys.options.eta_domain_scheme == 0
    [eta, ierr] = apply_malik_mapping(sys.options.N_eta, sys.options.eta_max, sys.options.eta_crit);
    if ierr
        fprintf('Reverting to an equally-spaced grid to avoid division by zero error!\n');
        %ierr = 0;
    end
elseif sys.options.eta_domain_scheme == 1
    % Equally-spaced grid
    eta = linspace(0, sys.options.eta_max, sys.options.N_eta).';
else
    error('InputArgumentError sys.options.eta_domain_scheme value is not supported');
end

% Set output struct
sys_out.beta = sys.beta;
sys_out.eta = eta;
sys_out.bc = sys.bc;
sys_out.options = sys.options;

end
