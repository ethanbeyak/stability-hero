function sys_out = os_setup_sys(sys)
%OS_SETUP_SYS Set defaults for various options
sys_out = sys;

% Topical options (cute)
sys = set_default(sys, 'silent', false);

% Pre-processing via appending uniform flow
sys = set_default(sys, 'append_uniform_flow', true);
sys = set_default(sys, 'append_interp_method', 'linear');

% Solving options
Ny = 200;
y_max = 75;
y_domain_scheme = 0;     % 0: malik-clustering; 1: equally-spaced
sys = set_default(sys, 'calculate_fro_norm', false);
sys = set_default(sys, 'Ny', Ny, ...
    ['Setting the y-domain points to ', num2str(Ny)]);
sys = set_default(sys, 'y_max', y_max, ...
    ['Setting the y-domain maximum to ', num2str(y_max)]);
sys = set_default(sys, 'y_domain_scheme', y_domain_scheme, ...
    'Setting the eta-domain scheme to malik-clustering.');
% Defaults specific to malik-clustering numerics
if (sys.y_domain_scheme == 0)
    y_crit = 4;
    sys = set_default(sys, 'y_crit', y_crit, ...
        ['Setting the malik-clustering critical parameter to ', num2str(y_crit)]);
end
sys = set_default(sys, 'eigs_sigma_search', 'sm');
sys = set_default(sys, 'nr_itmax', 200);
sys = set_default(sys, 'nr_tol', 1e-8);

% Post-processing via filtering
sys = set_default(sys, 'filter', true, ...
    'Setting sys.filter to true; mode filtering is on!');

% Plotting options
sys = set_default(sys, 'linewidth', 2);
sys = set_default(sys, 'fontsize', 14);
sys = set_default(sys, 'interpreter', 'latex');
sys = set_default(sys, 'markersize', 6);
% Only plot eigenfunctions by default if filtering is on...
sys = set_default(sys, 'plot_eigenfunctions', sys.filter);
sys = set_default(sys, 'plot_spectrum', sys.eigensolver ~= 2);
if sys.eigensolver == 2 && sys.plot_spectrum
    % There's no spectrum if doing Arnoldi. Override user-input!
    sys.plot_spectrum = false;
end

sys_out.silent = sys.silent;

sys_out.append_uniform_flow = sys.append_uniform_flow;
sys_out.append_interp_method = sys.append_interp_method;

sys_out.calculate_fro_norm = sys.calculate_fro_norm;
sys_out.Ny = sys.Ny;
sys_out.y_max = sys.y_max;
sys_out.y_domain_scheme = sys.y_domain_scheme;
if (sys.y_domain_scheme == 0)
    sys_out.y_crit = sys.y_crit;
end
sys_out.eigs_sigma_search = sys.eigs_sigma_search;
sys_out.nr_itmax = sys.nr_itmax;
sys_out.nr_tol = sys.nr_tol;

sys_out.filter = sys.filter;

sys_out.linewidth = sys.linewidth;
sys_out.fontsize = sys.fontsize;
sys_out.interpreter = sys.interpreter;
sys_out.markersize = sys.markersize;
sys_out.plot_eigenfunctions = sys.plot_eigenfunctions;
sys_out.plot_spectrum = sys.plot_spectrum;

end
