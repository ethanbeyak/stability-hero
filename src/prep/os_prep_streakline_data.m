function sys_out = os_prep_streakline_data(sys)

sys_out = sys;

% Extract locals
U_inf = sys.U_inf;              % m/s
streak_x0 = sys.streak_x0;      % m
f_ribbon = sys.f_ribbon;        % Hz, frequency of ribbon
nu = sys.nu;                    % m^2/s, kinematic viscosity
max_uprime_RMS_branchII_percent = sys.max_uprime_RMS_branchII_percent;  % Percentage of Uinf for A_II amplitude

% Build nondimensional parameters of the case at hand
F = 2*pi*f_ribbon*nu/(U_inf^2);
delta_r = sqrt(nu*streak_x0/U_inf);
R = U_inf*delta_r/nu;

% Values from Dr Reed's .xls file. See docs/
F_chart = [70; 80; 86; 90; 100]*1e-6;               % Nondim. frequency at branch I
R_I_chart = [510.1; 475.2; 457.5; 446.9; 423.4];    % sqrt(Re_x)
R_II_chart = [938.8; 849.6; 804.5; 777.2; 717.0];
maxlnAII_AI_chart = [3.00; 2.44; 2.16; 2.00; 1.55];
alpha_I_chart = [0.107; 0.112; 0.115; 0.116; 0.121];
alpha_II_chart = [0.191;0.194;0.196;0.197;0.199];

% Perform that "H" interpolation (a la Jeppesen's nomenclature) ...
% ... on the table for alpha guess at the given x. This is a workaround of 2D interpolation.
R_I = interp1(F_chart, R_I_chart, F);
R_II = interp1(F_chart, R_II_chart, F);
alpha_I = interp1(F_chart, alpha_I_chart, F);
alpha_II = interp1(F_chart, alpha_II_chart, F);

R_from_I_to_II = linspace(R_I, R_II);
alpha_from_I_to_II = linspace(alpha_I, alpha_II);
alpha_guess = interp1(R_from_I_to_II, alpha_from_I_to_II, R);

% Back calculate the amplitude at branch I at y = streak_y0 BASED ON u'
N_factor_II = interp1(F_chart, maxlnAII_AI_chart, F);
A_II_RMS = max_uprime_RMS_branchII_percent/100*U_inf;   % m/s
A_II = sqrt(2)*A_II_RMS;        % Undo the RMS
A_I = A_II*exp(-N_factor_II);   % m/s, inverse of e^N definition

% Then get a ROUGH estimate of N at this x location by interpolating between A_I and A_II
N_factor_from_I_to_II = linspace(0, N_factor_II);
N_factor = interp1(R_from_I_to_II, N_factor_from_I_to_II, R);
A = A_I*exp(N_factor);  % BASED ON u'

% Assign outputs
sys_out.F = F;
sys_out.delta_r = delta_r;
sys_out.R = R;
sys_out.alpha_guess = alpha_guess;
sys_out.A = A;

end
