function options = set_default(options, attr, value, varargin)
%SET_DEFAULT

if ((~isfield(options, attr)) || (isempty(options.(attr))))
    options.(attr) = value;
    if (nargin == 4)
        fprintf([varargin{1}, '\n']);
    end
end

end
