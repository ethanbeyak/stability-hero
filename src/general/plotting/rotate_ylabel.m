function rotate_ylabel(varargin)
%ROTATE_YLABEL Rotate ylabel of axes so that it is right-side up
% Usage:
%
% rotate_ylabel(); or rotate_ylabel(ylh);
%
% ylh: handle of the ylabel, obtained from ylh = get(gca,'ylabel');

if isempty(varargin)
    ylh = get(gca,'ylabel');
else
    ylh = varargin{1};
end
ylp = get(ylh, 'Position');
set(ylh, 'Rotation', 0, ...
    'Position', ylp, ...
    'VerticalAlignment', 'middle', ...
    'HorizontalAlignment','right');

end
