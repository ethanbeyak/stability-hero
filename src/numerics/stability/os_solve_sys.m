function sys_out = os_solve_sys(sys)
%OS_SOLVE_SYS

sys_out = sys;

% Extract locals
alpha = sys.alpha;
R = sys.R;
D1 = sys.D1;
D2 = sys.D2;
D4 = sys.D4;
d1_fd = sys.d1_fd;
d1_bd = sys.d1_bd;
U = sys.U;
dU_dy2 = sys.dU_dy2;
Ny = sys.Ny;
silent = sys.silent;

% Construct interior of left-hand-side and right-hand-side matrices
M_LHS = ...
    D4 + ...
    -2*alpha^2*D2 + ...
    eye(size(D2))*alpha^4 + ...
    -1i*R*alpha*U.*D2 + ... % location notation: Ui*D2ij
    1i*R*alpha^3*diag(U) + ...
    1i*R*alpha*diag(dU_dy2);
M_RHS = ...
    -1i*R*D2 + ...
    eye(size(D2))*1i*R*alpha^2;

% Apply boundary conditions
% Wall, Dirichlet (no-penetration)
M_LHS(1,:) = [1, zeros(1,Ny-1)];
M_RHS(1,:) = zeros(1,Ny);
% Wall, Neumann (no-slip)
M_LHS(2,:) = [d1_fd, zeros(1,Ny-length(d1_fd))];
M_RHS(2,:) = zeros(1,Ny);
% Freestream, Neumann (u decays)
M_LHS(Ny-1,:) = [zeros(1,Ny-length(d1_bd)), d1_bd];
M_RHS(Ny-1,:) = zeros(1,Ny);
% Freestream, Dirichlet (v decays)
M_LHS(Ny,:) = [zeros(1,Ny-1), 1];
M_RHS(Ny,:) = zeros(1,Ny);

% Solve the OSE: M_LHS*v = M_RHS*omega*v
if sys.eigensolver == 1         % global solver
    if ~silent
        fprintf('Solving the global generalized eigenvalue problem ...\n');
    end
    [M_v, M_omega] = eig(M_LHS, M_RHS);
elseif sys.eigensolver == 2     % local solver
    if ~silent
        fprintf('Solving the local generalized eigenvalue problem ...\n');
    end
    [M_v, M_omega] = eigs(M_LHS, M_RHS, sys.arnoldi_Ny, sys.eigs_sigma_search);
end
if sys.calculate_fro_norm
    sys_out.fro_norm = norm(M_LHS, 'fro');
end
M_cph = M_omega / alpha;

% Apply filtering if wanted, then
% Recreate u eigenfunction from continuity, i*alpha*u + Dv = 0
if sys.filter
    % Get logical mask for 0.2 < cph_r < 0.45
    M_filtered = real(M_cph) > 0.2 & real(M_cph) < 0.45;
    % Filter any modes with too large of a phase speed
    M_filtered = M_filtered & abs(imag(M_cph)) < 10;
    C_filtered = diag(M_filtered);
    M_v_filtered = M_v(:,C_filtered);
    C_cph_filtered = M_cph(M_filtered);
    
    % Check if all of the modes were filtered
    if size(M_v_filtered, 2) == 0
        if ~silent
            fprintf('Filtered out all of the modes! Output is now unfiltered.\n');
        end
        M_v_filtered = M_v;
        C_cph_filtered = diag(M_cph);
    end
    
    % Assign output
    v_hat_out = M_v_filtered;
    cph_out = C_cph_filtered;
else
    % Assign output
    v_hat_out = M_v;
    cph_out = diag(M_cph);
end

sys_out.u_hat = os_continuity_for_u(alpha, D1, v_hat_out);
sys_out.v_hat = v_hat_out;
sys_out.cph = cph_out;
sys_out.omega = diag(M_omega);
if sys.filter
    sys_out.u_hat_prefilter = os_continuity_for_u(alpha, D1, M_v);
    sys_out.v_hat_prefilter = M_v;
    sys_out.cph_prefilter = diag(M_cph);
end

end
