function M_u = os_continuity_for_u(alpha, D1, M_v)
%OS_CONTINUITY_FOR_U Apply the 2D continuity equation for Orr-Sommerfeld to
%solve for the u perturbation eigenfunction.

M_u = zeros(size(M_v));

for j = 1:size(M_v, 2)
    M_u(:,j) = 1i/alpha*D1*M_v(:,j);
end

end