function sys_out = os_prep_numerics(sys)
%OS_PREP_NUMERICS
%
% Create nondimensional U profile from the Falkner-Skan basic state
% Define the computational wall-normal grid (Malik-clusted/equally-spaced)
% Build differentiation matrices

sys_out = sys;

% Extract locals
m = sys.beta/(2 - sys.beta);
dyos_detafs = sqrt(2/(m+1));
detafs_dyos = 1/dyos_detafs;

% Create U and its second wall-normal derivative
if sys.append_uniform_flow
    % Define Orr-Sommerfeld grid
    y_max = sys.y_max;
    Ny = sys.Ny;
    if sys.y_domain_scheme == 0
        y_crit = sys.y_crit;
        [y_os, ~, y_eq, ...
            dy_eq_dy_os, dy_eq_dy_os2, dy_eq_dy_os3, dy_eq_dy_os4] = ...
            apply_malik_mapping(Ny, y_max, y_crit);
        dy = y_eq(2) - y_eq(1);
    elseif sys.y_domain_scheme == 1
        y_os = linspace(0, y_max, Ny).';  % column
        dy = y_os(2) - y_os(1);
    end
    % Extract subgrid without uniform flow extension
    y_os_fs = dyos_detafs*sys.eta;
    % Linearly interpolate the subgrid Falkner-Skan U and U'' onto the Orr-Sommerfeld subgrid
    % Set extrapolation values to uniform flow (U = 1, U'' = 0)
    U_inf = 1;
    dU_dy2_inf = 0;
    U = interp1(y_os_fs, sys.df_deta, y_os, sys.append_interp_method, U_inf);
    dU_dy2 = interp1(y_os_fs, detafs_dyos^2*sys.df_deta3, y_os, sys.append_interp_method, dU_dy2_inf);
else
    % The domain of Falkner-Skan is one-to-one with Orr-Sommerfeld's domain...
    % ...after accounting for the scaling sqrt(2/(m+1))
    y_os = dyos_detafs*sys.eta;
    Ny = length(y_os);
    dy = y_os(2) - y_os(1);
    U = sys.df_deta;
    dU_dy2 = detafs_dyos^2*sys.df_deta3; % Chain rule kicks in twice
end

% Build differentiation matrices
[D1_wrk, D2_wrk, D3_wrk, D4_wrk] = build_differentiation_matrices(Ny); % raw coefficients
d1_fd_wrk = 1/2*[-3, 4, -1];  % 0, 1, 2. 2nd order Stencil
d1_bd_wrk = -1*fliplr(d1_fd_wrk);
if sys.y_domain_scheme == 0
    % Apply the chain rule to the differentiation matrices (since we're using Malik clustering)
    D1 = apply_faa_di_bruno(1, D1_wrk/dy, dy_eq_dy_os);
    D2 = apply_faa_di_bruno(2, D2_wrk/dy^2, D1_wrk/dy, dy_eq_dy_os2, dy_eq_dy_os);
    D4 = apply_faa_di_bruno(4, D4_wrk/dy^4, D3_wrk/dy^3, D2_wrk/dy^2, D1_wrk/dy, ...
        dy_eq_dy_os4, dy_eq_dy_os3, dy_eq_dy_os2, dy_eq_dy_os);
    d1_fd_wrk = d1_fd_wrk*dy_eq_dy_os(1);
    d1_bd_wrk = d1_bd_wrk*dy_eq_dy_os(Ny);
else
    D1 = D1_wrk/dy;
    D2 = D2_wrk/dy^2;
    D4 = D4_wrk/dy^4;
end
d1_fd = d1_fd_wrk/dy;
d1_bd = d1_bd_wrk/dy;

% Ensure that U and U'' are oriented as columns (for products like U.*D2)
U = U(:);
dU_dy2 = dU_dy2(:);

% Assign output
sys_out.D1 = D1;
sys_out.D2 = D2;
sys_out.D4 = D4;
sys_out.d1_fd = d1_fd;
sys_out.d1_bd = d1_bd;
sys_out.U = U;
sys_out.dU_dy2 = dU_dy2;
sys_out.y = y_os;  % Note: SIMPLIFYING VARIABLE NAME ON ASSIGNMENT
sys_out.m = m;

end
