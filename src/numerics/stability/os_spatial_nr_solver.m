function sys_out = os_spatial_nr_solver(sys)
%OS_SPATIAL_NR_SOLVER Perform Newton-Raphson on the temporal problem until the spatial
% problem is solved, returning a complex alpha and the eigenfunctions u_hat & v_hat.

% Extract locals
alpha_guess = sys.alpha_guess;
R = sys.R;
F = sys.F;
nr_itmax = sys.nr_itmax;
nr_tol = sys.nr_tol;

iter = 1;
err_conv = 1;

sys_im1 = sys;          % Initialize the (i-1)th system
sys_im1.silent = true;  % Hush the stdout of os_solve_sys()
sys_im1.alpha = alpha_guess;

% Drive that real part of omega_r / R to F and the omega_i to zero through Newton-Raphson
while iter < nr_itmax && err_conv > nr_tol
    
    % Solve Orr-Sommerfeld
    sys_im1 = os_solve_sys(sys_im1);
    
    % Perform Newton-Raphson
    f = F*R - sys_im1.cph*sys_im1.alpha;    % Objective Newton-Raphson function, complex
    df_dalpha = -sys_im1.cph;               % analytical Jacobian
    alpha_i = sys_im1.alpha - f/df_dalpha;

    % Update for the next iteration
    err_conv = abs((alpha_i - sys_im1.alpha)/sys_im1.alpha);
    sys_im1.alpha = alpha_i;
    iter = iter + 1;
    
end

if iter == nr_itmax
    fprintf('Newton-Raphson reached max iterations: %i.\n', nr_itmax);
else
    fprintf('Newton-Raphson iterations: %i\n', iter);
end
fprintf('Relative error reached: %1.1e\n', err_conv);

% Assign output
sys_out = sys_im1;  % Loads in essential u_hat and v_hat fields, as well as other goodies ;)
sys_out.alpha = sys_im1.alpha;
sys_out.iter = iter;
sys_out.err_conv = err_conv;

end
