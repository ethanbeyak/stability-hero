function df_dx_out = apply_faa_di_bruno(order, varargin)
%APPLY_FAA_DI_BRUNO
% order = 1
%   df_dx = apply_faa_di_bruno(1, df_dg, dg_dx);
% order = 2
%   df_dx2 = apply_faa_di_bruno(2, df_dg2, df_dg, dg_dx2, dg_dx);
% order = 3
%   df_dx3 = apply_faa_di_bruno(3, df_dg3, df_dg2, df_dg, dg_dx3, dg_dx2, dg_dx);
% order = 4
%   df_dx4 = apply_faa_di_bruno(4, df_dg4, df_dg3, df_dg2, df_dg, dg_dx4, dg_dx3, dg_dx2, dg_dx);
%
% One may wish to take advantage of MATLAB's syntax of x.*A, where x is a vector size n and
% A is a square matrix, size n x n. Care is taken to make sure this syntax is respected by
% placing the matrix terms at the end of the multiplication products in the formulae below.
%
% 28 Oct 2018

i = 1;
switch order
    case 1
        df_dg = varargin{i}; i = i + 1;
        dg_dx = varargin{i}; %i = i + 1;
        df_dx_out = dg_dx.*df_dg;
    case 2
        df_dg2 = varargin{i}; i = i + 1;
        df_dg = varargin{i}; i = i + 1;
        dg_dx2 = varargin{i}; i = i + 1;
        dg_dx = varargin{i}; %i = i + 1;
        df_dx_out = dg_dx.^2.*df_dg2 + dg_dx2.*df_dg;
    case 3
        df_dg3 = varargin{i}; i = i + 1;
        df_dg2 = varargin{i}; i = i + 1;
        df_dg = varargin{i}; i = i + 1;
        dg_dx3 = varargin{i}; i = i + 1;
        dg_dx2 = varargin{i}; i = i + 1;
        dg_dx = varargin{i}; %i = i + 1;
        df_dx_out = dg_dx.^3.*df_dg3 + 3*dg_dx.*dg_dx2.*df_dg2 + dg_dx3.*df_dg;
    case 4
        df_dg4 = varargin{i}; i = i + 1;
        df_dg3 = varargin{i}; i = i + 1;
        df_dg2 = varargin{i}; i = i + 1;
        df_dg = varargin{i}; i = i + 1;
        dg_dx4 = varargin{i}; i = i + 1;
        dg_dx3 = varargin{i}; i = i + 1;
        dg_dx2 = varargin{i}; i = i + 1;
        dg_dx = varargin{i}; %i = i + 1;
        df_dx_out = dg_dx.^4.*df_dg4 + 6*dg_dx2.*dg_dx.^2.*df_dg3 + 3*dg_dx2.^2.*df_dg2 + ...
            4*dg_dx.*dg_dx3.*df_dg2 + dg_dx4.*df_dg;
    otherwise
        error("not supported yet!");
end

end
