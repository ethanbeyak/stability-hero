function [eta_cl, ierr, eta_equi, ...
    deta_equi_deta_cl, deta_equi_deta_cl2, deta_equi_deta_cl3, deta_equi_deta_cl4] = ...
    apply_malik_mapping(N_eta, eta_max, eta_crit)
%APPLY_MALIK_MAPPING

% Error integer
ierr = 0;

% Malik-mapping to cluster at eta_crit
eta_equi = linspace(0, 1, N_eta).';
a = eta_max * eta_crit / (eta_max - 2*eta_crit);
if any(isinf(a))
    % This formulation of Malik-mapping is not *kosher* for eta_max = 2*eta_crit
    ierr = 1;
    eta_cl = linspace(0, eta_max, N_eta).';
else
    b = 1 + a / eta_max;
    eta_cl = a*eta_equi ./ (b - eta_equi);
end

if nargout > 3
    deta_equi_deta_cl = b./(eta_cl+a)-(b*eta_cl)./(eta_cl+a).^2;
    deta_equi_deta_cl2 = (2*b*eta_cl)./(eta_cl+a).^3-(2*b)./(eta_cl+a).^2;
    deta_equi_deta_cl3 = (6*b)./(eta_cl+a).^3-(6*b*eta_cl)./(eta_cl+a).^4;
    deta_equi_deta_cl4 = (24*b*eta_cl)./(eta_cl+a).^5-(24*b)./(eta_cl+a).^4;
end

end
