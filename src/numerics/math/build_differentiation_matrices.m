function [D1, D2, D3, D4] = build_differentiation_matrices(N)
%BUILD_DIFFERENTIATION_MATRICES

D1 = zeros(N);
D2 = zeros(N);
D3 = zeros(N);
D4 = zeros(N);

% Source: http://web.media.mit.edu/~crtaylor/calculator.html
%% First derivative matrix
d1_fd = 1/2*[-3, 4, -1]; % Stencil: 0, 1, 2. 2nd order.
d1_cd = 1/2*[-1, 0, 1]; % Stencil: -1, 0, 1. 2nd order.
d1_bd = -1*fliplr(d1_fd); % Stencil: -2, -1, 0. 2nd order.

D1(1, 1:length(d1_fd)) = d1_fd;
for i = 2:N-1
    D1(i, i-1:i+1) = d1_cd;
end
D1(N, N-length(d1_fd)+1:N) = d1_bd;

%% Second derivative matrix
d2_fd = [2, -5, 4, -1]; % Stencil: 0, 1, 2, 3. 2nd order.
d2_cd = 1/12*[-1, 16, -30, 16, -1]; % Stencil: -2, -1, 0, 1, 2. 2nd order.
d2_bd = fliplr(d2_fd); % Stencil: -3, -2, -1, 0. 2nd order.

% Top
for i = 1:2
    D2(i, i:i+3) = d2_fd;
end
% Interior
for i = 3:N-2
    D2(i, i-2:i+2) = d2_cd;
end
% Bottom
for i = N-1:N
    D2(i, i-3:i) = d2_bd;
end

%% Third derivative matrix
d3_fd = 1/2*[-5, 18, -24, 14, -3]; % Stencil: 0, 1, 2, 3, 4. 2nd order.
d3_cd = 1/2*[-1, 2, 0, -2, 1]; % Stencil: -2, -1, 0, 1, -2. 2nd order.
d3_bd = -1*fliplr(d3_fd); % Stencil: -4, -3, -2, -1, 0. 2nd order.

% Top
for i = 1:2
    D3(i, i:i+4) = d3_fd;
end
% Interior
for i = 3:N-2
    D3(i, i-2:i+2) = d3_cd;
end
% Bottom
for i = N-1:N
    D3(i, i-4:i) = d3_bd;
end

%% Fourth derivative matrix
d4_fd = [3, -14, 26, -24, 11, -2]; % Stencil: 0, 1, 2, 3, 4, 5. 2nd order.
d4_cd = 1/6*[-1, 12, -39, 56, -39, 12, -1]; % Stencil: -3, -2, -1, 0, 1, 2, 3. 2nd order.
d4_bd = fliplr(d4_fd); % Stencil: 0, 1, 2, 3, 4, 5. 2nd order.

% Top
for i = 1:3
    D4(i, i:i+5) = d4_fd;
end
% Interior
for i = 4:N-3
    D4(i, i-3:i+3) = d4_cd;
end
% Bottom
for i = N-2:N
    D4(i, i-5:i) = d4_bd;
end

end
