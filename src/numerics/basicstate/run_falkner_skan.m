function [sys_out, eta_out, iter, step] = run_falkner_skan(sys, iter, ode_options, varargin)
%RUN_FALKNER_SKAN

% Initialize output
sys_out = sys;

if ~isempty(varargin)
    step = varargin{1};
else
    step = sys.ldf_deta2_wall(iter - 1) - sys.ldf_deta2_wall(iter - 2);
end

% Calculate solution
init_guess = [sys.bc.f_wall, sys.bc.df_deta_wall, sys.bc.df_deta2_wall];
[eta_out, sol] = ode45(@(eta, sol) falkner_skan(eta, sol, sys.beta), sys.eta, init_guess, ode_options);

% Store the solution
sys_out.f = sol(:,1);
sys_out.df_deta = sol(:,2);
sys_out.df_deta2 = sol(:,3);

% Store the freestream f' value and wall f'' value for NR convergence scheme
sys_out.ldf_deta_fs(iter) = sol(end,2);
sys_out.ldf_deta2_wall(iter) = sol(1,3);

% Increment f'' by a smidge
sys_out.bc.df_deta2_wall = sys.bc.df_deta2_wall + step;

% Finalize solution process
iter = iter + 1;

end
