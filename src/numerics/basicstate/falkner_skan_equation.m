function df_deta3 = falkner_skan_equation(beta, f, df_deta, df_deta2)
%FALKNER_SKAN_EQUATION

df_deta3 = -f.*df_deta2 - beta.*(1 - df_deta.^2);

end
