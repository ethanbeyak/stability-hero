function sys_out = fs_solve_sys(sys)
%SOLVE_SYS

ode_options = odeset('MaxStep',sys.eta(2) - sys.eta(1) ...
    , 'AbsTol', sys.options.tol ...
    , 'RelTol', sys.options.tol);        % deta
conv_f = 1;
iter = 1;
sys.bc.ldf_deta_wall = zeros(sys.options.iter_max,1);

fprintf('Solving the basic-state system with ode45.\n')

% Set up the first solution
% First two steps are just a tol smidge
[sys, eta_final, iter, ~] = run_falkner_skan(sys, iter, ode_options, sys.options.tol);

% Then repeat to initiate the NR convergence scheme
[sys, ~, iter, step] = run_falkner_skan(sys, iter, ode_options, sys.options.tol);

while conv_f > sys.options.tol && iter < sys.options.iter_max
    
    current_step = iter - 1;
    previous_step = current_step - 1;
    
    % Calculate the change (for NR) to drive solution toward f' = 1 at eta -> infinity
    df_deta_fs_nr = (sys.ldf_deta_fs(current_step) - sys.ldf_deta_fs(previous_step))/step;
    df_deta_fs_conv = sys.ldf_deta_fs(previous_step) - sys.bc.df_deta_fs;
    sys.bc.df_deta2_wall = sys.bc.df_deta2_wall - df_deta_fs_conv/df_deta_fs_nr;
    conv_f = abs(df_deta_fs_conv/sys.bc.df_deta_fs);
    
    % Get the updated solution with the f'' at the wall modified
    [sys, eta_final, iter, step] = run_falkner_skan(sys, iter, ode_options, step);
    
end

if (iter == sys.options.iter_max)
    fprintf('Reached the maximum iteration limit.\n');
end

% TODO: Check if solution satisfies all boundary conditions

fprintf(['Converged to %e within ', num2str(iter), ' iterations.\n'], conv_f);

sys_out = sys;
sys_out.df_deta3 = falkner_skan_equation(sys.beta, sys_out.f, sys_out.df_deta, sys_out.df_deta2);
sys_out.eta = eta_final;

end
