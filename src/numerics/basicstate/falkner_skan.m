function dsol_out = falkner_skan(eta, sol, beta) %#ok<INUSL>
%FALKNER_SKAN Decompose the Falkner-Skan equation into a system of three
% first-order, nonlinear, ordinary differential equations.
%
% f''' + f*f'' + beta*(1 - (f')^2) = 0
%
% sol: {f, df_deta, df_deta2}
% dsol_out: {df_deta, df_deta2, df_deta3}

% Unwrap input solution
f = sol(1);
df_deta = sol(2);
df_deta2 = sol(3);

% f''' for Falkner-Skan
df_deta3 = falkner_skan_equation(beta, f, df_deta, df_deta2);

% Output derivative of solution for ode45
dsol_out(1) = df_deta;
dsol_out(2) = df_deta2;
dsol_out(3) = df_deta3;
dsol_out = dsol_out(:);     % Ensure column output for ode45

end
