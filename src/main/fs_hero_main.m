function sys_out = fs_hero_main(sys)
%FS_HERO_MAIN Main function to calculate Falkner-Skan self-similar boundary-layer

tic;

sys = fs_setup_sys(sys);
sys = fs_solve_sys(sys);
sys_out = fs_finalize_sys(sys);

toc;

end
