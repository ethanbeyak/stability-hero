function sys_out = os_hero_main(sys, varargin)
%OS_HERO_MAIN Main function to solve the Orr-Sommerfeld equation
% for incompressible boundary-layer stability

if ~isempty(varargin)
    time_me = varargin{1};
else
    time_me = true;
end

if time_me, tic, end

sys = os_setup_sys(sys);
sys = os_prep_numerics(sys);
sys = os_solve_sys(sys);
sys_out = os_finalize_sys(sys);

if time_me, toc, end

end
