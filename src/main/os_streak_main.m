function sys_out = os_streak_main(sys)
%OS_STREAK_MAIN Main function to solve the Orr-Sommerfeld equation
% for incompressible Tollmien-Schlichting waves and plot the streaklines

tic;

sys = os_setup_sys(sys);
sys = os_prep_streakline_data(sys);
sys = os_prep_numerics(sys);
sys = os_spatial_nr_solver(sys);
sys_out = os_finalize_streak(sys);

toc;

end
